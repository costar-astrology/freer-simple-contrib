module Control.Monad.Freer.Mock.HList where

data HList xs where
  HNil :: HList '[]
  HCons :: x -> HList xs -> HList (x ': xs)

instance Eq (HList '[]) where
  _ == _ = True

instance (Eq x, Eq (HList xs)) => Eq (HList (x ': xs)) where
  HCons x xs == HCons y ys = x == y && xs == ys



