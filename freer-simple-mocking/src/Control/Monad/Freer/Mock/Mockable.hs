{-# LANGUAGE TemplateHaskell #-}

module Control.Monad.Freer.Mock.Mockable  where

import           Control.Monad
import           Control.Monad.Freer.Mock.HList
import           Control.Monad.Freer.Mock.Types
import           Data.Typeable
import           Language.Haskell.TH

data SomeType where
  SomeType :: (Typeable a) => Proxy a -> HList (Args a) -> SomeType

class Mockable f where
  promote :: f a -> SomeType

deriveMockable :: Name -> Q [Dec]
deriveMockable name = [d|
  instance Mockable $a where
    promote = $derivePromote
 |]

  where
    a = conT name

    gadtCons :: [Con] -> [(Name, [Type])]
    gadtCons []                       = []
    gadtCons (GadtC names bngTypes _ : rest) = namesAndTypes ++ gadtCons rest
      where namesAndTypes = [(conName, snd <$> bngTypes)
                            | conName <- names
                            ]
    gadtCons (RecGadtC names bngTypes _ : rest) = namesAndTypes ++ gadtCons rest
      where namesAndTypes = [ (conName, (\(_, _, ty) -> ty) <$> bngTypes)
                            | conName <- names
                            ]
    gadtCons _ = fail "deriveMockable only works on GADT constructors"

    derivePromote :: Q Exp
    derivePromote = do
      info <- reify name
      case info of
        TyConI (DataD _ _ _ _ cons _) -> do
          x <- newName "x"
          matches <- forM (gadtCons cons) $ \(con, tys) -> do
            args <- sequence $ newName "arg" <$ tys
            let promotedCon = return $ PromotedT con
            let hlist = return $ foldr (\n e -> AppE (AppE (ConE 'HCons) (VarE n)) e) (ConE 'HNil) args
            body <- [| SomeType (Proxy @($promotedCon)) $hlist |]
            return $ Match (ConP con (VarP <$> args)) (NormalB body) []
          return $ LamE [VarP x] (CaseE (VarE x) matches)
        _ -> fail "deriveMockable only works on GADT constructors"
