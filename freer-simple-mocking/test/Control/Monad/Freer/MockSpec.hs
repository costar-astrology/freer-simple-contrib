{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeInType      #-}

module Control.Monad.Freer.MockSpec where

import           Control.Monad.Freer
import           Control.Monad.Freer.Mock
import           Test.Hspec

data X = X deriving Eq

data T a where
  T1 :: Int -> Bool -> String -> T Bool
  T2 :: X -> String -> T Int
  T3 :: T [String]

deriveMockable ''T

spec :: Spec
spec = do
  describe "T" $ do
    it "returns it's own values" $ do
      let res = run . runT $ send T3
      res `shouldBe` []

  describe "mock" $ do
    it "can overload a value" $ do
      let res = run . runT . mock [given @'T3 :=> ["new"]] $ send T3
      res `shouldBe` ["new"]

    it "can override multiple values" $ do
      let res = run .runT . mock [given @'T3 :=> ["new"]
                                 ,matching @'T1 Any Any Any :=> True
                                 ] $ (,) <$> send T3 <*> send (T1 0 False "")
      res `shouldBe` (["new"], True)

    it "can respond to specific mocks" $ do
      let res = run .runT . mock [given @'T1 0 False "" :=> True] $
            (,) <$> send (T1 1 False "") <*> send (T1 0 False "")
      res `shouldBe` (False, True)

    it "can match" $ do
      let res = run .runT . mock [matching @'T1 (Exactly 0) Any Any :=> True] $
            (,,) <$> send (T1 0 True "xyz") <*> send (T1 0 False "") <*> send (T1 1 False "")
      res `shouldBe` (True, True, False)

runT :: forall r v . Eff (T ': r) v -> Eff r v
runT = interpret handle
  where
    handle :: T a -> Eff r a
    handle T1{} = return False
    handle T2{} = return 3
    handle T3{} = return []

